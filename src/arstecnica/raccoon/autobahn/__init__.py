# -*- coding: utf-8 -*-
# :Project:   arstecnica.raccoon.autobahn -- A set of common utilities over Autobahn
# :Created:   ven 25 set 2015, 11:20:05, CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015, 2017 Arstecnica s.r.l.
#
