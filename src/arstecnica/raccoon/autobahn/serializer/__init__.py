# -*- coding: utf-8 -*-
# :Project:   arstecnica.raccoon.autobahn -- Custom serializers for Autobahn
# :Created:   mer 25 nov 2015 11:01:11 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015, 2017 Arstecnica s.r.l.
#
