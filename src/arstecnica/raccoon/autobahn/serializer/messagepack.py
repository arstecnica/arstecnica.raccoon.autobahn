# -*- coding: utf-8 -*-
# :Project:   arstecnica.raccoon.autobahn -- MessagePack custom serializer
# :Created:   mer 25 nov 2015 11:11:37 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015, 2017 Arstecnica s.r.l.
#

"""
Custom MessagePack serializer
=============================
"""

import datetime, decimal, struct, uuid

import msgpack

from autobahn.wamp.interfaces import IObjectSerializer, ISerializer
from autobahn.wamp.serializer import Serializer


NAIVE_DATETIME_TYPE = 41, "!hBBBBBI"  # Year,Month,Day,Hour,Min,Sec,Microsec
TZ_DATETIME_TYPE    = 42, "!hBBBBBIi" # Year,Month,Day,Hour,Min,Sec,Microsec,Offset
NAIVE_TIME_TYPE     = 43, "!BBBI"     # Hour,Min,Sec,Microsec
TZ_TIME_TYPE        = 44, "!BBBIi"    # Hour,Min,Sec,Microsec,Offset
DATE_TYPE           = 45, "!hBB"      # Year,Month,Day
UUID_TYPE           = 46              # 16-bytes-string
DECIMAL_TYPE        = 47              # ascii-string


def _default(obj,
             _exttype=msgpack.ExtType,
             _pack=struct.pack,
             _date=datetime.date,
             _date_type=DATE_TYPE,
             _time=datetime.time,
             _naive_time_type=NAIVE_TIME_TYPE,
             _tz_time_type=TZ_TIME_TYPE,
             _datetime=datetime.datetime,
             _naive_datetime_type=NAIVE_DATETIME_TYPE,
             _tz_datetime_type=TZ_DATETIME_TYPE,
             _uuid=uuid.UUID,
             _uuid_type=UUID_TYPE,
             _decimal=decimal.Decimal,
             _decimal_type=DECIMAL_TYPE):
    if isinstance(obj, _datetime):
        if obj.tzinfo is None:
            return _exttype(_naive_datetime_type[0],
                            _pack(_naive_datetime_type[1],
                                  obj.year, obj.month, obj.day,
                                  obj.hour, obj.minute, obj.second,
                                  obj.microsecond))
        else:
            utcoffset = obj.utcoffset()
            secoffset = utcoffset.days * 24*60*60 + utcoffset.seconds
            return _exttype(_tz_datetime_type[0],
                            _pack(_tz_datetime_type[1],
                                  obj.year, obj.month, obj.day,
                                  obj.hour, obj.minute, obj.second,
                                  obj.microsecond, secoffset))

    elif isinstance(obj, _date):
        return _exttype(_date_type[0],
                        _pack(_date_type[1], obj.year, obj.month, obj.day))

    elif isinstance(obj, _time):
        if obj.tzinfo is None:
            return _exttype(_naive_time_type[0],
                            _pack(_naive_time_type[1],
                                  obj.hour, obj.minute, obj.second,
                                  obj.microsecond))
        else:
            utcoffset = obj.utcoffset()
            secoffset = utcoffset.days * 24*60*60 + utcoffset.seconds
            return _exttype(_tz_time_type[0],
                            _pack(_tz_time_type[1],
                                  obj.hour, obj.minute, obj.second,
                                  obj.microsecond, secoffset))

    elif isinstance(obj, _uuid):
        return _exttype(_uuid_type, obj.bytes)

    elif isinstance(obj, _decimal):
        return _exttype(_decimal_type, str(obj).encode('ascii'))

    raise TypeError("Unknown type: %r" % (obj,))


def _ext_hook(code, data,
              _exttype=msgpack.ExtType,
              _unpack=struct.unpack,
              _timezone=datetime.timezone,
              _timedelta=datetime.timedelta,
              _date=datetime.date,
              _date_type=DATE_TYPE,
              _time=datetime.time,
              _naive_time_type=NAIVE_TIME_TYPE,
              _tz_time_type=TZ_TIME_TYPE,
              _datetime=datetime.datetime,
              _naive_datetime_type=NAIVE_DATETIME_TYPE,
              _tz_datetime_type=TZ_DATETIME_TYPE,
              _uuid=uuid.UUID,
              _uuid_type=UUID_TYPE,
              _decimal=decimal.Decimal,
              _decimal_type=DECIMAL_TYPE):
    if code == _naive_datetime_type[0]:
        return _datetime(*_unpack(_naive_datetime_type[1], data))

    elif code == _tz_datetime_type[0]:
        data = _unpack(_tz_datetime_type[1], data)
        tz = _timezone(_timedelta(seconds=data[-1]))
        return _datetime(*data[:-1], tz)

    elif code == _date_type[0]:
        return _date(*_unpack(_date_type[1], data))

    elif code == _naive_time_type[0]:
        return _time(*_unpack(_naive_time_type[1], data))

    elif code == _tz_time_type[0]:
        data = _unpack(_tz_time_type[1], data)
        tz = _timezone(_timedelta(seconds=data[-1]))
        return _time(*data[:-1], tz)

    elif code == _uuid_type:
        return _uuid(bytes=data)

    elif code == _decimal_type:
        return _decimal(data.decode('ascii'))

    return _exttype(code, data)


def packb(data):
    return msgpack.packb(data, default=_default, encoding='utf-8', use_bin_type=True)


def unpackb(data):
    return msgpack.unpackb(data, ext_hook=_ext_hook, encoding='utf-8', use_list=False)


class MsgPackObjectSerializer(object):
    "Implement raccoon's custom MessagePack serialization."

    BINARY = True
    """
    Flag that indicates whether this serializer needs a binary clean transport.
    """

    def __init__(self, batched=False):
        """
        Ctor.

        :param batched: Flag that controls whether serializer operates in batched mode.
        :type batched: bool
        """
        self._batched = batched

    def serialize(self, obj):
        """
        Implements :func:`autobahn.wamp.interfaces.IObjectSerializer.serialize`
        """
        data = packb(obj)
        if self._batched:
            return struct.pack("!L", len(data)) + data
        else:
            return data

    def unserialize(self, payload):
        """
        Implements :func:`autobahn.wamp.interfaces.IObjectSerializer.unserialize`
        """

        if self._batched:
            msgs = []
            N = len(payload)
            i = 0
            while i < N:
                # read message length prefix
                if i + 4 > N:
                    raise Exception("batch format error [1]")
                l = struct.unpack("!L", payload[i:i + 4])[0]

                # read message data
                if i + 4 + l > N:
                    raise Exception("batch format error [2]")
                data = payload[i + 4:i + 4 + l]

                # append parsed raw message
                msgs.append(unpackb(data))

                # advance until everything consumed
                i = i + 4 + l

            if i != N:
                raise Exception("batch format error [3]")
            return msgs

        else:
            unpacked = unpackb(payload)
            return [unpacked]

IObjectSerializer.register(MsgPackObjectSerializer)


class MsgPackSerializer(Serializer):
    "Custom MessagePack serializer for Autobahn."

    SERIALIZER_ID = "rmsgpack"
    """
    ID used as part of the WebSocket subprotocol name to identify the
    serializer with WAMP-over-WebSocket.
    """

    RAWSOCKET_SERIALIZER_ID = 3
    """
    ID used in lower four bits of second octet in RawSocket opening
    handshake identify the serializer with WAMP-over-RawSocket.
    """

    MIME_TYPE = "application/x-rmsgpack"
    """
    MIME type announced in HTTP request/response headers when running
    WAMP-over-Longpoll HTTP fallback.
    """

    def __init__(self, batched=False):
        """
        Ctor.

        :param batched: Flag to control whether to put this serialized into batched mode.
        :type batched: bool
        """
        Serializer.__init__(self, MsgPackObjectSerializer(batched=batched))
        if batched:
            self.SERIALIZER_ID = "rmsgpack.batched"

ISerializer.register(MsgPackSerializer)


if __name__ == '__main__':
    now = datetime.datetime.now()
    utc = datetime.datetime.now(datetime.timezone.utc)
    ny = utc.astimezone(datetime.timezone(datetime.timedelta(hours=-5)))
    data = ({'date': now.date(), 'time': now.time(), 'datetime': now, 'utc': utc, 'ny': ny},
            uuid.uuid1(),
            "foo", b'\002',
            decimal.Decimal('3.1415926'))
    print(data)
    packed = packb(data)
    print(packed)
    unpacked = unpackb(packed)
    print(unpacked)
    print(unpacked == data)
    print([c for c in packed])
