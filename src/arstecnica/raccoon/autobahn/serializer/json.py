# -*- coding: utf-8 -*-
# :Progetto:  arstecnica.raccoon.autobahn -- JSON serializer replacement for Autobahn
# :Creato:    mar 07 lug 2015 22:29:34 CEST
# :Autore:    Lele Gaifax <lele@metapensiero.it>
# :Licenza:   GNU General Public License version 3 or later
#

"""
Custom JSON serializer
======================
"""

import decimal, uuid

import nssjson
from nssjson import JSONDecoder, JSONEncoder

from autobahn.wamp.interfaces import IObjectSerializer, ISerializer
from autobahn.wamp.serializer import Serializer


class NssJsonObjectSerializer(object):
    """Implement JSON serialization thru `nssjson`__.

    Use ``nssjson`` capabilities to implement transparent
    serialization and deserialization of Python's ``date``,
    ``datetime``, ``Decimal`` and ``UUID`` instances.

    __ https://pypi.python.org/pypi/nssjson

    ::

      >>> import datetime, decimal, uuid
      >>> id = uuid.uuid3(uuid.NAMESPACE_OID, "example")
      >>> pi = decimal.Decimal("3.14159")
      >>> bd = datetime.date(1968, 3, 18)
      >>> print(dumps(id))
      "16eda250-39c7-3c5d-b2f8-7eb6dfedff40"
      >>> print(dumps(bd))
      "1968-03-18"
      >>> print(dumps(pi))
      3.14159

    ::

      >>> d = [id, bd, pi]
      >>> loads('["16eda250-39c7-3c5d-b2f8-7eb6dfedff40","1968-03-18",3.14159]') == d
      True
    """

    JSON_MODULE = nssjson
    BINARY = False

    def __init__(self, batched=False, serialize_hook=None, unserialize_hook=None):
        """
        Ctor.

        :param batched: flag that controls whether serializer operates in
                        batched mode
        :type batched: bool
        """

        self._batched = batched
        self._encoder = JSONEncoder(separators=(',', ':'),
                                    use_decimal=True,
                                    iso_datetime=True,
                                    handle_uuid=True,
                                    default=serialize_hook).encode
        self._decoder = JSONDecoder(parse_float=decimal.Decimal,
                                    iso_datetime=True,
                                    handle_uuid=True,
                                    object_hook=unserialize_hook).decode


    def serialize(self, obj):
        """
        Implements :func:`autobahn.wamp.interfaces.IObjectSerializer.serialize`
        """
        s = self._encoder(obj)
        if isinstance(s, str):
            s = s.encode('utf-8')
        if self._batched:
            return s + b'\30'
        else:
            return s

    def unserialize(self, payload):
        """
        Implements :func:`autobahn.wamp.interfaces.IObjectSerializer.unserialize`
        """
        if self._batched:
            chunks = payload.split(b'\30')[:-1]
        else:
            chunks = [payload]
        if len(chunks) == 0:
            raise Exception("batch format error")
        decoder = self._decoder
        return [decoder(data.decode('utf-8')) for data in chunks]


IObjectSerializer.register(NssJsonObjectSerializer)


class NssJsonSerializer(Serializer):
    "Custom JSON serializer for Autobahn."

    SERIALIZER_ID = "json"
    RAWSOCKET_SERIALIZER_ID = 1
    MIME_TYPE = "application/json"

    def __init__(self, batched=False,
                 serialize_hook=None, unserialize_hook=None):
        """
        Ctor.

        :param batched: flag to control whether to put this serialized into
                        batched mode
        :type batched: bool
        :param serialize_hook: if given, a callable accepting a Python
                               instance and returning a JSON serializable
                               object if possible, otherwise raising an
                               exception
        :type serialize_hook: ``None`` or callable
        :param unserialize_hook: if given, a callable accepting a Python
                                 dictionary and returning a Python instance
                                 if possible, otherwise the same dictionary
        :type unserialize_hook: ``None`` or callable

        """
        Serializer.__init__(self, NssJsonObjectSerializer(batched,
                                                          serialize_hook,
                                                          unserialize_hook))
        if batched:
            self.SERIALIZER_ID = "json.batched"


ISerializer.register(NssJsonSerializer)


def main():
    from datetime import date
    serializer = NssJsonObjectSerializer()
    s = serializer.serialize([date.today(), uuid.uuid1(), "foo",
                              decimal.Decimal('3.1415926')])
    print(s)
    print(serializer.unserialize(s))

if __name__ == '__main__':
    main()
