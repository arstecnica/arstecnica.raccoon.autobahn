.. -*- coding: utf-8 -*-
.. :Project:   arstecnica.raccoon.autobahn -- Documentation
.. :Created:   dom 18 dic 2016 14:31:40 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   No License
.. :Copyright: © 2016, 2017 Arstecnica s.r.l.
..

=====================================
 Common utilities on top of Autobahn
=====================================

.. toctree::

   client
   serializers
