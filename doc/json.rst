.. -*- coding: utf-8 -*-
.. :Project:   arstecnica.raccoon.autobahn -- JSON serializer documentation
.. :Created:   dom 15 gen 2017 13:36:35 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   No License
.. :Copyright: © 2017 Arstecnica s.r.l.
..

=================
 JSON serializer
=================

.. automodule:: arstecnica.raccoon.autobahn.serializer.json
   :members:
