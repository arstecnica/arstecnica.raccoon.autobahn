.. -*- coding: utf-8 -*-
.. :Project:   arstecnica.raccoon.autobahn -- MessagePack serializer documentation
.. :Created:   dom 15 gen 2017 13:37:34 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   No License
.. :Copyright: © 2017 Arstecnica s.r.l.
..

========================
 MessagePack serializer
========================

.. automodule:: arstecnica.raccoon.autobahn.serializer.messagepack
   :members:
