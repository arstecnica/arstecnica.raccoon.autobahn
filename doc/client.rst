.. -*- coding: utf-8 -*-
.. :Project:   arstecnica.raccoon.autobahn -- Client documentation
.. :Created:   dom 15 gen 2017 13:34:12 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   No License
.. :Copyright: © 2017 Arstecnica s.r.l.
..

========
 Client
========

.. automodule:: arstecnica.raccoon.autobahn.client
   :members:
