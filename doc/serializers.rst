.. -*- coding: utf-8 -*-
.. :Project:   arstecnica.raccoon.autobahn -- Serializers documentation
.. :Created:   dom 15 gen 2017 13:35:29 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   No License
.. :Copyright: © 2017 Arstecnica s.r.l.
..

=============
 Serializers
=============

.. toctree::

   json
   messagepack
