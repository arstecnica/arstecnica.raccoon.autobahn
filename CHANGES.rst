.. -*- coding: utf-8 -*-

Changes
-------

0.4 (2015-10-31)
~~~~~~~~~~~~~~~~

- Internal tweak, forward the debug flags to the transport factory


0.3 (2015-10-23)
~~~~~~~~~~~~~~~~

- Fix issue with the asyncio loop and txaio


0.2 (2015-09-25)
~~~~~~~~~~~~~~~~

- Minor packaging tweaks


0.1 (2015-09-25)
~~~~~~~~~~~~~~~~

- Renamed to arstecnica.raccoon.autobahn (to workaround https://github.com/pypa/pip/issues/3)


0.0 (unreleased)
~~~~~~~~~~~~~~~~

- Initial effort.
